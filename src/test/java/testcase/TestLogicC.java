package testcase;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import logic.Calculation;

public class TestLogicC {
	
	@BeforeClass  
    public static void setUpBeforeClass() throws Exception {  
        System.out.println("before class: Invoked only onec before starting all test");  
    }  
    @Before  
    public void setUp() throws Exception {  
        System.out.println("before: Invoked before each test");  
    }  
    
	@Test
	public void testFindMax() {
		  assertEquals(7,Calculation.findMax(new int[]{1,3,7,2,5,1,4})); 
//		  assertEquals(-1,Calculation.findMax(new int[]{-12,-1,-3,-4,-2})); 
		  System.out.println("First test successfully  run");
	}
	
	@Test
	public void testCube() {
		assertEquals(27,Calculation.cube(3));
		 System.out.println("Second test successfully  run");
	}
	
	@After  
    public void tearDown() throws Exception {  
        System.out.println("after: Invoked after each test");  
    } 
	
	@AfterClass  
    public static void tearDownAfterClass() throws Exception {  
        System.out.println("after class: Invoked only onces after finishing all test");  
    } 

}
